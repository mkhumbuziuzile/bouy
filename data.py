import time
from datetime import datetime
from datetime import date
from scipy import fft

def enviroment():
    print("Collecting data for Temparature, Humidity and Pressure\n")
    shtc3 = SHTC3()
    f = input("Enter the file name:\n")
    file = open(f,"w")
    samples = int(input("Enter the number of samples:\n"))
    rate = float(input("Enter the sampling rate:\n"))
    print("Writing Temparature and Pressure values into file")
    temps = []
    hums = []
    pres = []

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    today = date.today()
    today = str(today.strftime("%B %d, %Y"))

    for i in range (0,samples):
        temp = round(shtc3.SHTC3_Read_Temperature(),3)
        hum  = round(shtc3.SHTC3_Read_Humidity(),3)
        pre = pressure()
        temps.append(temp)
        hums.append(hum)
        pres.append(pre)
        time.sleep(rate)

    file.write(today+"\n")
    file.write(current_time+"\n")

    file.write("sampling rate = "+str(rate)+" seconds\n")
    file.write("Temparature in degrees:\n")
    file.write(str(temps))
    file.write("\nHumidity in %:\n")
    file.write(str(hums))
    file.write("\nPressure in hPa:\n")
    file.write(str(pres))
    file.close()

def movement():
    print("Collecting data for Accelerometer, Gyroscope and Magnetometer\n")
    f = input("Enter the file name:\n")
    file = open(f,"w")
    samples = int(input("Enter the number of samples:\n"))
    rate = float(input("Enter the sampling rate:\n"))
    print("\nWEritting movement sensors to file\n")
    icm20948=ICM20948()
    #Acceleration:
    Ax = []
    Ay = []
    Az = []

    #Gyroscope:
    Gx = []
    Gy = []
    Gz = []

    #Magnetic:
    Mx = []
    My = []
    Mz = []

    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    today = date.today()
    today = str(today.strftime("%B %d, %Y"))

    for i in range (0,samples):
        icm20948.icm20948_Gyro_Accel_Read()
        icm20948.icm20948MagRead()
        icm20948.icm20948CalAvgValue()
        icm20948.imuAHRSupdate(MotionVal[0] * 0.0175, MotionVal[1] * 0.0175,MotionVal[2] * 0.0175,
                MotionVal[3],MotionVal[4],MotionVal[5], 
                MotionVal[6], MotionVal[7], MotionVal[8])
        pitch = math.asin(-2 * q1 * q3 + 2 * q0* q2)* 57.3
        roll  = math.atan2(2 * q2 * q3 + 2 * q0 * q1, -2 * q1 * q1 - 2 * q2* q2 + 1)* 57.3
        yaw   = math.atan2(-2 * q1 * q2 - 2 * q0 * q3, 2 * q2 * q2 + 2 * q3 * q3 - 1) * 57.3

        #Acceleration:
        Ax.append(Accel[0])
        Ay.append(Accel[1])
        Az.append(Accel[2])

        #Gyroscope:
        Gx.append(Gyro[0])
        Gy.append(Gyro[1])
        Gz.append(Gyro[2])

        #Magnetic:
        Mx.append(Mag[0])
        My.append(Mag[1])
        Mz.append(Mag[2])
        time.sleep(rate)
    AX = fft(Ax)
    AY = fft(Ay)
    AZ = fft(Az)

    GX = fft(Gx)
    GY = fft(Gy)
    GZ = fft(Gz)

    MX = fft(Mx)
    MY = fft(My)
    MZ = fft(Mz)

    file.write(today+"\n")
    file.write(current_time+"\n")
    file.write("sampling rate = "+str(rate)+" seconds\n")
    file.write("#")
    file.write("\nFFT of Accelaration:\n")
    file.write("x-axis:\n"+str(AX)+"\n")
    file.write("y-axis:\n"+str(AY)+"\n")
    file.write("z-axis:\n"+str(AZ)+"\n")
    file.write("#")

    file.write("\nFFT of Gyroscope:\n")
    file.write("\nx-axis:\n"+str(GX)+"\n")
    file.write("\ny-axis:\n"+str(GY)+"\n")
    file.write("\nz-axis:\n"+str(GZ)+"\n")
    file.write("#")

    file.write("\nFFT of Magnetic:\n")
    file.write("\nx-axis:\n"+str(MX)+"\n")
    file.write("\ny-axis:\n"+str(MY)+"\n")
    file.write("\nz-axis:\n"+str(MZ)+"\n")
    file.write("#") 
    file.close()
    #print("\r\n /-------------------------------------------------------------/ \r\n")
    #print('\r\n Roll = %.2f , Pitch = %.2f , Yaw = %.2f\r\n'%(roll,pitch,yaw))
    #print('\r\nAcceleration:  X = %d , Y = %d , Z = %d\r\n'%(Accel[0],Accel[1],Accel[2]))  
    #print('\r\nGyroscope:     X = %d , Y = %d , Z = %d\r\n'%(Gyro[0],Gyro[1],Gyro[2]))
    #print('\r\nMagnetic:      X = %d , Y = %d , Z = %d'%((Mag[0]),Mag[1],Mag[2]))

if __name__ == "__main__":
    enviroment()
    movement()